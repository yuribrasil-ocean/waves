# WAVES

Projeto que reúne diversos rotinas para calcular parâmetros oriundos do espectro direcional ou mesmo do espectro unidirectional de ondas e para plotar gráficos do espectro de ondas 2D e 1D de diversas formas.

# Programas

[spec_ftheta_plot.py]

Programa que contém as seguintes rotinas:

- omnidirectional_spec(dir_vec, freq_vec, my_spec, param_dict, fname, norm_flag, peak_flag, param_flag, vec_flag, fig_title)

- polar_spec(dir_vec, freq_vec, spec2d, param_dict, fname, ftick, arrow_flag, radius_flag, norm_flag, peak_flag, param_flag, vec_flag, fig_title, fhalf=False)

- cartesian_spec(dir_vec, freq_vec, spec2d, param_dict, fname, norm_flag, peak_flag, param_flag, vec_flag, fig_title)

- map_spec(dir_vec, freq_vec, spec2d, param_dict, fname, norm_flag, peak_flag, param_flag, vec_flag, fig_title)

omnidirectional_spec cria um gráfico 1D do espectro de ondas com uma linha tracejada vertical sobre a frequência de pico. O eixo horizontal representa o vetor de frequências ou períodos e o vertical, a energia (m²/Hz). O usuário deve fornecer os vetores de direção (dir_vec), em graus; de frequência (freq_vec), em Hz; o espectro de ondas (my_spec), um dicionário com os parâmetros de pico a serem plotados (param_dict), o nome do arquivo que será salvo (fname); o valor booleano para normalizar a escala de energia (norm_flag), o valor booleano para plotar a linha tracejada sobre o pico (peak_flag), valor booleano para plotar os parâmetros (param_flag), string que serve como flag para definir se o vetor será de frequências ('freq') ou de períodos ('per') (vec_flag) e o título da figura (fig_title).

polar_spec cria um gráfico polar do espectro direcional de ondas. Os círculos concêntricos representam os valores de frequência ou período, as linhas radiais, a direção. O usuário deve fornecer os vetores de direção (dir_vec), em graus; de frequência (freq_vec), em Hz; o espectro de ondas (spec2d), um dicionário com os parâmetros de pico a serem plotados (param_dict), o nome do arquivo que será salvo (fname); a lista com os ticks dos valores de frequência (ftick), valor booleano para plotar o eixo vertical e horizontal em formato de seta (arrow_flag); o valor booleano para plotar os valores das frequências ou períodos (radius_flag); o valor booleano para normalizar a escala de energia (norm_flag), o valor booleano para plotar um 'x' sobre o pico (peak_flag), valor booleano para plotar os parâmetros (param_flag), string que serve como flag para definir se o vetor será de frequências ('freq') ou de períodos ('per') (vec_flag) e o título da figura (fig_title).

cartesian_spec cria um gráfico cartesiano do espectro direcional de ondas. O eixo horizontal representa o vetor de frequências ou períodos, o eixo vertical, a direção. O usuário deve fornecer os vetores de direção (dir_vec), em graus; de frequência (freq_vec), em Hz; o espectro de ondas (spec2d), um dicionário com os parâmetros de pico a serem plotados (param_dict), o nome do arquivo que será salvo (fname); o valor booleano para normalizar a escala de energia (norm_flag), o valor booleano para plotar um 'x' sobre o pico (peak_flag), valor booleano para plotar os parâmetros (param_flag), string que serve como flag para definir se o vetor será de frequências ('freq') ou de períodos ('per') (vec_flag) e o título da figura (fig_title).

map_spec cria um gráfico do espectro direcional de ondas em formato de mapa de calor, plotando apenas os valores acima de 0. O eixo horizontal representa o vetor de frequências ou períodos, o eixo vertical, a direção. O usuário deve fornecer os vetores de direção (dir_vec), em graus; de frequência (freq_vec), em Hz; o espectro de ondas (spec2d), um dicionário com os parâmetros de pico a serem plotados (param_dict), o nome do arquivo que será salvo (fname); o valor booleano para normalizar a escala de energia (norm_flag), o valor booleano para plotar um quadrado de destaque sobre o pico (peak_flag), valor booleano para plotar os parâmetros (param_flag), string que serve como flag para definir se o vetor será de frequências ('freq') ou de períodos ('per') (vec_flag) e o título da figura (fig_title).

[spec_parameters.py]

Programa que contém as seguintes rotinas: 

- hs_spec(freq_vec, dir_vec, spec, two_dim_flag, log_flag)

- peak_parameters_max(freq_vec, dir_vec, spec, two_dim_flag, log_flag)

- peak_parameters_int(freq_dim, freq_vec, dir_dim, dir_vec, spec, log_flag)

hs_spec calcula o valor de Hm0 (Hs) seguindo o cálculo de 4 vezes a raíz quadrada da integral do espectro direcional (2D) ou do espectro unidirectional (1D). Para esclarecer o tipo de espectro, o usuário deve prover o valor booleano (two_dim_flag) True ou False para espectro direcional ou unidrecional, respectivamente. O usuário deve prover também o valor booleano (log_flag), que indica se o vetor de frequências é em escala logarítmica (True) ou normal (False). Para finalizar, o usuário deve fornecer os vetores de frequência (freq_vec), em Hz; de direção (dir_vec), em graus e o próprio espectro de ondas (spec).

peak_parameters_max calcula os parâmetros do espectro buscando as coordenadas do valor máximo do espectro 2D. Necessita dos vetores de frequência (freq_vec), em Hz; de direção (dir_vec), em graus; o espectro de ondas (spec) e o valor booleando log_flag.

peak_parameters_int calcula os parâmetros do espectro buscando a partir do pico de frequência do espectro unidirecional de onda. Necessita do índice da dimensão do vetor de frequências (freq_dim); do vetor de direções (dir_dim), dos vetores de frequência (freq_vec), em Hz; de direção (dir_vec), em graus; o espectro de ondas (spec) e o valor booleano log_flag. Primeiro, a rotina cria o espectro unidirecional de ondas a partir da integral do espectro 2D e busca pelo índice do valor máximo. Em seguida, busca-se o índice da célula de maior valor no espectro 2D com apenas a banda da frequência de pico.

# Próximos passos

- Criar uma rotina para os parâmetros de valor médio.


