#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

# Script: plot_example.py

# Author: Yuri Brasil - yuri.brasil@oceanica.ufrj.br

# Created on Sun Nov 26 17:17:10 2023

# Modification: November 26th 2023

# Objective: Read and plot a wave spectrum (f,theta)

# Functions:
            
"""

# import os
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
import spec_ftheta_plot as sp 
import spec_parameters as par

# Reading the file
my_file = 'my_matrices_envisat_512pts_jonswap_unimodal_nonlinear_2.5m_45deg_406m.mat'
# my_file = 'my_matrices_envisat_512pts_jonswap_unimodal_nonlinear_2.5m_40deg_350m.mat'
output = sio.loadmat(my_file)

# Getting the variables as arrays
freq_vec = output['freq_vec'][0]
dir_vec = output['dir_vec'][0]
theta_vec = np.deg2rad(dir_vec)
df = output['df'][0]
dtheta = np.radians(360/len(output['dir_vec'][0]))
    
spec2d = output['spec_2D']
string_flag = 'Hs = 2.5m / Lp = 406m / Dp = 45°'
# string_flag = 'Hs = 2.5m / Lp = 350m / Dp = 40°'

# Calculate Hs and peak parameters
hs = par.hs_spec(freq_vec, dir_vec, spec2d, True, True)
fp, tp, lp, dp = par.peak_parameters_max(freq_vec, dir_vec, spec2d, True)
fp_int, tp_int, lp_int, dp_int = par.peak_parameters_int(1, freq_vec, 0, dir_vec, spec2d, True)
    
# Param dictionary
param_dict = {'Hs':hs,'Fp':fp,'Tp':tp,'Lp':lp,'Dp':dp}
param_dict2 = {'Hs':hs,'Fp':fp_int,'Tp':tp_int,'Lp':lp_int,'Dp':dp_int}

# Geeting the maximum value
maximum_energy = np.max(spec2d)

# set frequency grid values (radius)
ftick = [0.05, 0.10, 0.15, 0.20]

# Flag to plot 0º-180º and 90º-270º Arrows (Polar plot)
arrow_flag = True

# Flag to plot radius labels (Polar plot)
radius_flag = True

# Flag for the normalization 
norm_flag = False

# Flag for mark the spectrum peak
peak_flag = True

# Flag to plot the wave parameters in a text box
param_flag = True

# Flag for the type of Wave Spectrum ('freq'=(f,Theta) or 'per'=(t,theta))
vec_flag = 'freq'

for j in ['omni', 'polar', 'cartesian', 'map']:

    # Flag for the type of plot ('polar', 'cartesian' and 'map')
    plot_flag = j
    
    # Setting figure title
    # fig_title = 'Wave Spectrum Density - Image ' + n_image + ' ' + string_flag
    fig_title = 'Wave Spectrum Density ' + string_flag
    
    # Setting normalization string
    if norm_flag == True:
        norm_str = '_norm'    
    else:    
        norm_str = ''
    
    # Setting the filename    
    fname = plot_flag + '_wave_spec_' + vec_flag + \
            norm_str + '.png'
            
    # The Plots
    
    if plot_flag == 'omni':
        sp.omnidirectional_spec(dir_vec, freq_vec, spec2d, param_dict2, fname, norm_flag,
                             peak_flag, param_flag, vec_flag, fig_title)
    
    if plot_flag == 'polar':
        sp.polar_spec(dir_vec, freq_vec, spec2d, param_dict2, fname, ftick, arrow_flag, 
                   radius_flag, norm_flag, peak_flag, param_flag, vec_flag, fig_title)
        
    elif plot_flag == 'cartesian':
        sp.cartesian_spec(dir_vec, freq_vec, spec2d, param_dict2, fname, norm_flag, peak_flag,
                       param_flag, vec_flag, fig_title)
        
    elif plot_flag == 'map':
        sp.map_spec(dir_vec, freq_vec, spec2d, param_dict2, fname, norm_flag, peak_flag,
                 param_flag, vec_flag, fig_title)
            
plt.close('all')    