#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 16 13:05:39 2023

Script/Function: spec_parameters.py

Author: Yuri Brasil

e-mail: yuri.brasil@oceanica.ufrj.br

Modification: June 6th 2023

Objective: Calculate significant wave height (Hs or SWH), peak parameters such 
           as peak frequency, peak period and peak direction (based on maximum
           value of the 2D spectrum or the maximum of a integrated 1D 
           spectrum).

"""

###############################################################################

import numpy as np

############## Calculating the Significant Wave Height (Hs) ###################

def hs_spec(freq_vec, dir_vec, spec, two_dim_flag, log_flag):
    
# Calculating the df vector
    if log_flag == True:
    
        df = np.zeros(len(freq_vec))
        df[0] = (freq_vec[1] - freq_vec[0]/1.1)/2
        
        # Loop to calculate the df elements (except the first and the last ones)
        for i in range(1,len(freq_vec)-1):
            df[i] = (freq_vec[i+1]-freq_vec[i-1])/2
    
        df[-1] = (freq_vec[-1]*1.1 - freq_vec[-2])/2
        
    elif log_flag == False:
        
        df = freq_vec[1]-freq_vec[0]
                    
# Calculating the dtheta element
    dtheta = np.deg2rad(dir_vec[1]-dir_vec[0])        
    
# Calculating the Hs
    if two_dim_flag == True:
        hs = 4*np.sqrt(np.sum(np.sum(spec*df)*dtheta))
    elif two_dim_flag == False:
        hs = 4*np.sqrt(np.sum(spec*df))
    
    return hs

######### Calculating the peak parameters (coordinates of maximum) ############

def peak_parameters_max(freq_vec, dir_vec, spec, log_flag):
    
# Calculating the df vector
    if log_flag == True:
    
        df = np.zeros(len(freq_vec))
        df[0] = (freq_vec[1] - freq_vec[0]/1.1)/2
        
        # Loop to calculate the df elements (except the first and the last ones)
        for i in range(1,len(freq_vec)-1):
            df[i] = (freq_vec[i+1]-freq_vec[i-1])/2
            
        # Calculate the last element    
        df[-1] = (freq_vec[-1]*1.1 - freq_vec[-2])/2
        
    elif log_flag == False:
        
        df = freq_vec[1]-freq_vec[0]

# Calculate the direction vector with the center of direction range
    dtheta_half = (dir_vec[1]-dir_vec[0])/2
    dir_center = dir_vec - dtheta_half
    dir_last = dir_center[0] + 360
    dir_center = np.concatenate((dir_center[1:len(dir_center)],[dir_last]))
    
# Create a frequency vector with the center of the frequency band    
    df_half = df/2
    new_freq = np.concatenate(([0],freq_vec[0:len(freq_vec)-1]))
    freq_center = df_half + new_freq

# Calculate the peak frequency, period and wavelength    
    [y, x] = np.where(spec == np.max(spec))
    fp = freq_vec[x[0]]
    # fp = freq_center[x[0]]
    tp = 1/fp
    lp = 1.56*tp**2
    dp = dir_vec[y[0]]
    # dp = dir_center[y[0]]
          
    return fp, tp, lp, dp

####### Calculating the peak parameters (integration of the spectrum) #########

def peak_parameters_int(freq_dim, freq_vec, dir_dim, dir_vec, spec, log_flag):
    
# Calculate the df vector
    if log_flag == True:
    
        df = np.zeros(len(freq_vec))
        df[0] = (freq_vec[1] - freq_vec[0]/1.1)/2
        
        # Loop to calculate the df elements (except the first and the last ones)
        for i in range(1,len(freq_vec)-1):
            df[i] = (freq_vec[i+1]-freq_vec[i-1])/2
    
        df[-1] = (freq_vec[-1]*1.1 - freq_vec[-2])/2
        
    elif log_flag == False:
        
        df = freq_vec[1]-freq_vec[0]
                    
# Calculate the dtheta element
    dtheta = np.deg2rad(dir_vec[1]-dir_vec[0])
    
# Calculate the direction vector with the center of direction range
    dtheta_half = (dir_vec[1]-dir_vec[0])/2
    dir_center = dir_vec - dtheta_half
    dir_last = dir_center[0] + 360
    dir_center = np.concatenate((dir_center[1:len(dir_center)],[dir_last]))
    
# Create a frequency vector with the center of the frequency band    
    df_half = df/2
    new_freq = np.concatenate(([0],freq_vec[0:len(freq_vec)-1]))
    freq_center = df_half + new_freq
    
# Calculate the peak frequency, period and wavelength
    spec_freq = np.sum(spec*dtheta,axis=dir_dim)
    
    [x] = np.where(spec_freq == np.max(spec_freq))
    fp = freq_vec[x[0]]
    # fp = freq_center[x[0]]
    tp = 1/fp
    lp = 1.56*tp**2

# Calculate the peak direction    
    peak_band_spec = spec[:,x[0]]
    
    [y] = np.where(peak_band_spec == np.max(peak_band_spec))  
    
    dp = dir_vec[y[0]]
    # dp = dir_center[y[0]]
       
    return fp, tp, lp, dp







    


